fetch('https://jsonplaceholder.typicode.com/todos')
	.then(response =>  response.json())
	.then(data =>{
		let mapList = data.map(title => ({title:title.title}))
			console.log(mapList)
	});


// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

	fetch('https://jsonplaceholder.typicode.com/todos/1' {
  	
		.then(response =>  response.json())
		.then(data => console.log(`Title:${data.title} Status ${data.dateCompleted}`);
	});

// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.


// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
	fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	//Sets the content/body of the 'request' object to be sent to the backend
	body: JSON.stringify({
		title:'New To Do Post',
		completed: 'true',
		userId: 2
	})
	})
	.then(res => res.json())
	.then(data => console.log(data))

// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

	fetch('https://jsonplaceholder.typicode.com/posts/3', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		//Sets the content/body of the 'request' object to be sent to the backend
		body: JSON.stringify({
			title:'New To Do Put',
		completed: 'true',
		userId: 3
		})
	})
	.then(res => res.json())
	.then(data => console.log(data))
// 9. Update a to do list item by changing the data structure to contain the following properties:
// - Title
// - Description
// - Status
// - Date Completed
// - User ID
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	//Sets the content/body of the 'request' object to be sent to the backend
	body: JSON.stringify({
		title:'New Post',
		description: 'Hello Again',
		status: 'active',
		dateCompleted: '12/03/12',
		userId: 1

		})
	})
	.then(res => res.json())
	.then(data => console.log(data))
// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	//Sets the content/body of the 'request' object to be sent to the backend
	body: JSON.stringify({
		title:'New Post',
		description: 'Hello Again',
		status: 'active',
		dateCompleted: '12/03/12',
		userId: 1

		})
	})
	.then(res => res.json())
	.then(data => console.log(data))
// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.
	fetch('https://jsonplaceholder.typicode.com/posts/6', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	//Sets the content/body of the 'request' object to be sent to the backend
	body: JSON.stringify({
		
		status: 'active',
		dateCompleted: '12/03/12',
		

		})
	})
	.then(res => res.json())
	.then(data => console.log(data))
// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
	fetch('https://jsonplaceholder.typicode.com/posts/5', {
	method: 'DELETE',
	})
	.then(res => res.json())
	.then(data => console.log(data))
